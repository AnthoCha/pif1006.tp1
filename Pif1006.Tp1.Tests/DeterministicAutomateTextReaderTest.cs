using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pif1006.Tp1.IO;
using System.IO;
using System.Text;

namespace Pif1006.Tp1.Tests
{
    [TestClass]
    public class DeterministicAutomateTextReaderTest
    {
        [TestMethod]
        public void Read_WithTextReaderNull_ReturnsNull()
        {
            // Arrange
            var reader = new DeterministicAutomateTextReader(null);

            // Act
            var automate = reader.ReadAutomate();

            // Assert
            Assert.AreEqual(null, automate);
        }

        [TestMethod]
        public void Read_WithEmptyText_ReturnsInitialStateNull()
        {
            // Arrange
            var sb = new StringBuilder();

            using var textReader = new StringReader(sb.ToString());
            var reader = new DeterministicAutomateTextReader(textReader);

            // Act
            var automate = reader.ReadAutomate();

            // Assert
            Assert.AreEqual(null, automate.InitialState);
        }

        [TestMethod]
        public void Read_WithInvalidState_ReturnsInitialStateNull()
        {
            // Arrange
            var sb = new StringBuilder();
            sb.AppendLine("transition s0 0 s1");

            using var textReader = new StringReader(sb.ToString());
            var reader = new DeterministicAutomateTextReader(textReader);

            // Act
            var automate = reader.ReadAutomate();

            // Assert
            Assert.AreEqual(null, automate.InitialState);
        }

        [TestMethod]
        public void Read_WithInvalidToken_ReturnsInitialStateNull()
        {
            // Arrange
            var sb = new StringBuilder();
            sb.AppendLine("invalid s0 0 s1");

            using var textReader = new StringReader(sb.ToString());
            var reader = new DeterministicAutomateTextReader(textReader);

            // Act
            var automate = reader.ReadAutomate();

            // Assert
            Assert.AreEqual(null, automate.InitialState);
        }

        [TestMethod]
        [DataRow("", false)]
        [DataRow("1", true)]
        [DataRow("10", true)]
        [DataRow("100", true)]
        [DataRow("1000", true)]
        [DataRow("10000", true)]
        [DataRow("0", false)]
        [DataRow("01", false)]
        [DataRow("11", false)]
        [DataRow("a", false)]
        public void Read_IsValid_WithExample1(string input, bool expected)
        {
            // Arrange
            var sb = new StringBuilder();
            sb.AppendLine("state s0 0");
            sb.AppendLine("state s1 1");
            sb.AppendLine("transition s0 1 s1");
            sb.AppendLine("transition s1 0 s1");

            using var textReader = new StringReader(sb.ToString());
            var reader = new DeterministicAutomateTextReader(textReader);

            // Act
            var automate = reader.ReadAutomate();
            var actual = automate.IsValid(input);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [DataRow("", true)]
        [DataRow("10", true)]
        [DataRow("1010", true)]
        [DataRow("101010", true)]
        [DataRow("10101010", true)]
        [DataRow("0", false)]
        [DataRow("1", false)]
        [DataRow("01", false)]
        [DataRow("11", false)]
        [DataRow("a", false)]
        public void Read_IsValid_WithExample2(string input, bool expected)
        {
            // Arrange
            var sb = new StringBuilder();
            sb.AppendLine("state s0 1");
            sb.AppendLine("state s1 0");
            sb.AppendLine("transition s0 1 s1");
            sb.AppendLine("transition s1 0 s0");

            using var textReader = new StringReader(sb.ToString());
            var reader = new DeterministicAutomateTextReader(textReader);

            // Act
            var automate = reader.ReadAutomate();
            var actual = automate.IsValid(input);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [DataRow("", true)]
        [DataRow("100", true)]
        [DataRow("100100", true)]
        [DataRow("100100100", true)]
        [DataRow("100100100100", true)]
        [DataRow("0", false)]
        [DataRow("1", false)]
        [DataRow("01", false)]
        [DataRow("10", false)]
        [DataRow("11", false)]
        [DataRow("a", false)]
        public void Read_IsValid_WithExample3(string input, bool expected)
        {
            // Arrange
            var sb = new StringBuilder();
            sb.AppendLine("state s0 1");
            sb.AppendLine("state s1 0");
            sb.AppendLine("state s2 0");
            sb.AppendLine("transition s0 1 s1");
            sb.AppendLine("transition s1 0 s2");
            sb.AppendLine("transition s2 0 s0");

            using var textReader = new StringReader(sb.ToString());
            var reader = new DeterministicAutomateTextReader(textReader);

            // Act
            var automate = reader.ReadAutomate();
            var actual = automate.IsValid(input);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [DataRow("", false)]
        [DataRow("0", true)]
        [DataRow("01", true)]
        [DataRow("1", false)]
        [DataRow("10", false)]
        [DataRow("11", false)]
        [DataRow("a", false)]
        public void Read_IsValid_WithExample4(string input, bool expected)
        {
            // Arrange
            var sb = new StringBuilder();
            sb.AppendLine("state s0 0");
            sb.AppendLine("state s1 1");
            sb.AppendLine("state s2 1");
            sb.AppendLine("transition s0 0 s1");
            sb.AppendLine("transition s1 1 s2");

            using var textReader = new StringReader(sb.ToString());
            var reader = new DeterministicAutomateTextReader(textReader);

            // Act
            var automate = reader.ReadAutomate();
            var actual = automate.IsValid(input);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [DataRow("", false)]
        [DataRow("0", true)]
        [DataRow("00", true)]
        [DataRow("01", true)]
        [DataRow("000", true)]
        [DataRow("001", true)]
        [DataRow("011", true)]
        [DataRow("1", false)]
        [DataRow("10", false)]
        [DataRow("11", false)]
        [DataRow("a", false)]
        public void Read_IsValid_WithExample5(string input, bool expected)
        {
            // Arrange
            var sb = new StringBuilder();
            sb.AppendLine("state s0 0");
            sb.AppendLine("state s1 1");
            sb.AppendLine("transition s0 0 s1");
            sb.AppendLine("transition s1 0 s1");
            sb.AppendLine("transition s1 1 s1");

            using var textReader = new StringReader(sb.ToString());
            var reader = new DeterministicAutomateTextReader(textReader);

            // Act
            var automate = reader.ReadAutomate();
            var actual = automate.IsValid(input);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [DataRow("", true)]
        [DataRow("1", true)]
        [DataRow("01", true)]
        [DataRow("001", true)]
        [DataRow("11", true)]
        [DataRow("111", true)]
        [DataRow("0011", true)]
        [DataRow("1111", true)]
        [DataRow("0", false)]
        [DataRow("10", false)]
        [DataRow("a", false)]
        public void Read_IsValid_WithExample6(string input, bool expected)
        {
            // Arrange
            var sb = new StringBuilder();
            sb.AppendLine("state s0 1");
            sb.AppendLine("state s1 0");
            sb.AppendLine("state s2 1");
            sb.AppendLine("transition s0 0 s1");
            sb.AppendLine("transition s0 1 s2");
            sb.AppendLine("transition s1 0 s1");
            sb.AppendLine("transition s1 1 s2");
            sb.AppendLine("transition s2 0 s1");
            sb.AppendLine("transition s2 1 s2");

            using var textReader = new StringReader(sb.ToString());
            var reader = new DeterministicAutomateTextReader(textReader);

            // Act
            var automate = reader.ReadAutomate();
            var actual = automate.IsValid(input);

            // Assert
            Assert.AreEqual(expected, actual);
        }
    }
}
