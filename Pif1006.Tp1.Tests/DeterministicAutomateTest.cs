using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pif1006.Tp1.Core;

namespace Pif1006.Tp1.Tests
{
    [TestClass]
    public class DeterministicAutomateTest
    {
        [TestMethod]
        [DataRow("")]
        [DataRow("0")]
        [DataRow("1")]
        [DataRow("01")]
        [DataRow("10")]
        [DataRow("11")]
        [DataRow("a")]
        public void IsValid_WithInitialStateNull_ReturnsFalse(string input)
        {
            // Arrange
            var automate = new DeterministicAutomate(null);

            // Act
            var result = automate.IsValid(input);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        [DataRow("", false)]
        [DataRow("1", true)]
        [DataRow("10", true)]
        [DataRow("100", true)]
        [DataRow("1000", true)]
        [DataRow("10000", true)]
        [DataRow("0", false)]
        [DataRow("01", false)]
        [DataRow("11", false)]
        [DataRow("a", false)]
        public void IsValid_WithExample1(string input, bool expected)
        {
            // Arrange
            var s0 = new State(false);
            var s1 = new State(true);

            s0.Transitions.Add(new Transition('1', s1));
            s1.Transitions.Add(new Transition('0', s1));

            var automate = new DeterministicAutomate(s0);

            // Act
            var actual = automate.IsValid(input);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [DataRow("", true)]
        [DataRow("10", true)]
        [DataRow("1010", true)]
        [DataRow("101010", true)]
        [DataRow("10101010", true)]
        [DataRow("0", false)]
        [DataRow("1", false)]
        [DataRow("01", false)]
        [DataRow("11", false)]
        [DataRow("a", false)]
        public void IsValid_WithExample2(string input, bool expected)
        {
            // Arrange
            var s0 = new State(true);
            var s1 = new State(false);

            s0.Transitions.Add(new Transition('1', s1));
            s1.Transitions.Add(new Transition('0', s0));

            var automate = new DeterministicAutomate(s0);

            // Act
            var actual = automate.IsValid(input);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [DataRow("", true)]
        [DataRow("100", true)]
        [DataRow("100100", true)]
        [DataRow("100100100", true)]
        [DataRow("100100100100", true)]
        [DataRow("0", false)]
        [DataRow("1", false)]
        [DataRow("01", false)]
        [DataRow("10", false)]
        [DataRow("11", false)]
        [DataRow("a", false)]
        public void IsValid_WithExample3(string input, bool expected)
        {
            // Arrange
            var s0 = new State(true);
            var s1 = new State(false);
            var s2 = new State(false);

            s0.Transitions.Add(new Transition('1', s1));
            s1.Transitions.Add(new Transition('0', s2));
            s2.Transitions.Add(new Transition('0', s0));

            var automate = new DeterministicAutomate(s0);

            // Act
            var actual = automate.IsValid(input);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [DataRow("", false)]
        [DataRow("0", true)]
        [DataRow("01", true)]
        [DataRow("1", false)]
        [DataRow("10", false)]
        [DataRow("11", false)]
        [DataRow("a", false)]
        public void IsValid_WithExample4(string input, bool expected)
        {
            // Arrange
            var s0 = new State(false);
            var s1 = new State(true);
            var s2 = new State(true);

            s0.Transitions.Add(new Transition('0', s1));
            s1.Transitions.Add(new Transition('1', s2));

            var automate = new DeterministicAutomate(s0);

            // Act
            var actual = automate.IsValid(input);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [DataRow("", false)]
        [DataRow("0", true)]
        [DataRow("00", true)]
        [DataRow("01", true)]
        [DataRow("000", true)]
        [DataRow("001", true)]
        [DataRow("011", true)]
        [DataRow("1", false)]
        [DataRow("10", false)]
        [DataRow("11", false)]
        [DataRow("a", false)]
        public void IsValid_WithExample5(string input, bool expected)
        {
            // Arrange
            var s0 = new State(false);
            var s1 = new State(true);

            s0.Transitions.Add(new Transition('0', s1));
            s1.Transitions.Add(new Transition('0', s1));
            s1.Transitions.Add(new Transition('1', s1));

            var automate = new DeterministicAutomate(s0);

            // Act
            var actual = automate.IsValid(input);

            // Assert
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [DataRow("", true)]
        [DataRow("1", true)]
        [DataRow("01", true)]
        [DataRow("001", true)]
        [DataRow("11", true)]
        [DataRow("111", true)]
        [DataRow("0011", true)]
        [DataRow("1111", true)]
        [DataRow("0", false)]
        [DataRow("10", false)]
        [DataRow("a", false)]
        public void IsValid_WithExample6(string input, bool expected)
        {
            // Arrange
            var s0 = new State(true);
            var s1 = new State(false);
            var s2 = new State(true);

            s0.Transitions.Add(new Transition('0', s1));
            s0.Transitions.Add(new Transition('1', s2));
            s1.Transitions.Add(new Transition('0', s1));
            s1.Transitions.Add(new Transition('1', s2));
            s2.Transitions.Add(new Transition('0', s1));
            s2.Transitions.Add(new Transition('1', s2));

            var automate = new DeterministicAutomate(s0);

            // Act
            var actual = automate.IsValid(input);

            // Assert
            Assert.AreEqual(expected, actual);
        }
    }
}
