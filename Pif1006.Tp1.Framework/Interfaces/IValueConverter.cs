﻿namespace Pif1006.Tp1.Framework.Interfaces
{
    /// <summary>
    /// Représente un convertisseur entre le type source et le type de destination.
    /// </summary>
    /// <typeparam name="TSource">Type source.</typeparam>
    /// <typeparam name="TDest">Type de destination.</typeparam>
    public interface IValueConverter<TSource, TDest>
    {
        /// <summary>
        /// Convertit du type source vers le type de destination.
        /// </summary>
        /// <param name="source">Valeur du type source.</param>
        /// <returns>Retourne la valeur convertit du type de destination.</returns>
        TDest ConvertTo(TSource source);

        /// <summary>
        /// Convertit du type de destination vers le type source.
        /// </summary>
        /// <param name="source">Valeur du type de destination.</param>
        /// <returns>Retourne la valeur convertit du type source.</returns>
        TSource ConvertFrom(TDest dest);
    }
}
