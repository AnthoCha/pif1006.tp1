﻿using Pif1006.Tp1.Framework.Interfaces;

namespace Pif1006.Tp1.Framework
{
    /// <summary>
    /// Représente un convertisseur d'une valeur booléenne vers sa représentation en chaine comme bit.
    /// </summary>
    public class BooleanToBitStringConverter : IValueConverter<bool, string>
    {
        /// <summary>
        /// Représente en chaine comme bit de la valeur booléenne true.
        /// </summary>
        public const string True = "1";
        /// <summary>
        /// Représente en chaine comme bit de la valeur booléenne false.
        /// </summary>
        public const string False = "0";

        /// <inheritdoc/>
        public string ConvertTo(bool source)
        {
            return source ? True : False;
        }

        /// <inheritdoc/>
        public bool ConvertFrom(string dest)
        {
            return dest == True;
        }
    }
}
