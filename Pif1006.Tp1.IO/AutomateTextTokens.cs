﻿namespace Pif1006.Tp1.IO
{
    /// <summary>
    /// Jetons des automates sous forme de texte.
    /// </summary>
    public class AutomateTextTokens
    {
        /// <summary>
        /// Séparateur de jetons.
        /// </summary>
        public const string TokenSeparator = " ";
        /// <summary>
        /// Jeton d'état.
        /// </summary>
        public const string StateToken = "state";
        /// <summary>
        /// Jeton de transition.
        /// </summary>
        public const string TransitionToken = "transition";
    }
}
