﻿using Pif1006.Tp1.Core;
using Pif1006.Tp1.Core.Interfaces;
using Pif1006.Tp1.Framework;
using Pif1006.Tp1.Framework.Interfaces;
using System.Collections.Generic;
using System.IO;

namespace Pif1006.Tp1.IO
{
    /// <summary>
    /// Représente un lecteur de texte capable de lire un automate.
    /// </summary>
    /// <remarks>
    /// Un état doit être défini avant ses transitions.
    /// Si un état est défini une seconde fois, il écrase l'état précédent.
    /// Le nom des états ne sont pas mémorisés après la lecture de l'automate.
    /// </remarks>
    public abstract class AutomateTextReader : IAutomateReader
    {
        private readonly TextReader _reader;
        private readonly IValueConverter<bool, string> _boolConverter;

        /// <summary>
        /// Initalise une nouvelle instance de la classe <see cref="AutomateTextReader"/> avec le lecteur de texte spécifié
        /// et une nouvelle instance de <see cref="BooleanToBitStringConverter"/> comme convertisseur de valeurs.
        /// </summary>
        /// <param name="reader">Lecteur de texte.</param>
        public AutomateTextReader(TextReader reader)
            : this(reader, new BooleanToBitStringConverter())
        {
        }

        /// <summary>
        /// Initalise une nouvelle instance de la classe <see cref="AutomateTextReader"/> avec le lecteur de texte spécifié
        /// et le convertisseur de valeurs spécifié.
        /// </summary>
        /// <param name="reader">Lecteur de texte.</param>
        /// <param name="boolConverter">Convertisseur de valeurs.</param>
        public AutomateTextReader(TextReader reader, IValueConverter<bool, string> boolConverter)
        {
            _reader = reader;
            _boolConverter = boolConverter ?? new BooleanToBitStringConverter();
        }

        /// <inheritdoc/>
        public IAutomate ReadAutomate()
        {
            if (_reader == null)
            {
                return null;
            }

            var namedStates = new Dictionary<string, State>();
            State initialState = null;
            State state;

            string currentLine;
            string[] tokens;

            while ((currentLine = _reader.ReadLine()) != null)
            {
                tokens = currentLine.Split(AutomateTextTokens.TokenSeparator);

                if (tokens.Length > 0)
                {
                    switch (tokens[0])
                    {
                        case AutomateTextTokens.StateToken:
                            if (tokens.Length > 2)
                            {
                                state = new State(_boolConverter.ConvertFrom(tokens[2]));
                                namedStates[tokens[1]] = state;
                                initialState ??= state;
                            }
                            break;
                        case AutomateTextTokens.TransitionToken:
                            if (tokens.Length > 3)
                            {
                                if (namedStates.TryGetValue(tokens[1], out var fromState)
                                    && namedStates.TryGetValue(tokens[3], out var toState))
                                {
                                    _ = char.TryParse(tokens[2], out var input);
                                    fromState.Transitions.Add(new Transition(input, toState));
                                }
                            }
                            break;
                    }
                }
            }

            return CreateAutomate(initialState);
        }

        /// <summary>
        /// Crée un automate concret avec l'état initial spécifié.
        /// </summary>
        /// <param name="initialState">État initial de l'automate.</param>
        /// <returns>Retourne l'automate concret avec l'état initial spécifié.</returns>
        protected abstract IAutomate CreateAutomate(State initialState);
    }
}
