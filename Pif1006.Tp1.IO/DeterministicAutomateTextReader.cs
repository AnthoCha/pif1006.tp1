﻿using Pif1006.Tp1.Core;
using Pif1006.Tp1.Core.Interfaces;
using System.IO;

namespace Pif1006.Tp1.IO
{
    /// <summary>
    /// Représente un lecteur de texte capable de lire un automate déterministe.
    /// </summary>
    /// <remarks>
    /// Un état doit être défini avant ses transitions.
    /// Si un état est défini une seconde fois, il écrase l'état précédent.
    /// Le nom des états ne sont pas mémorisés après la lecture de l'automate.
    /// </remarks>
    public class DeterministicAutomateTextReader : AutomateTextReader
    {

        /// <summary>
        /// Initalise une nouvelle instance de la classe <see cref="DeterministicAutomateTextReader"/> avec le lecteur de texte spécifié.
        /// </summary>
        /// <param name="reader">Lecteur de texte.</param>
        public DeterministicAutomateTextReader(TextReader reader)
            : base(reader)
        {
        }

        /// <inheritdoc/>
        protected override IAutomate CreateAutomate(State initialState)
        {
            return new DeterministicAutomate(initialState);
        }
    }
}
