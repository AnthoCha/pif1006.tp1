﻿using Pif1006.Tp1.Core;
using Pif1006.Tp1.Core.Interfaces;
using Pif1006.Tp1.Framework;
using Pif1006.Tp1.Framework.Interfaces;
using System.Collections.Generic;
using System.IO;

namespace Pif1006.Tp1.IO
{
    /// <summary>
    /// Représente un writer de texte capable d'écrire un automate.
    /// </summary>
    /// <remarks>
    /// Les états orphelins et leurs transitions ne sont pas écrits car ils ne sont pas accessibles par un simple parcours de l'automate.
    /// Lors de l'écriture, les états sont assignés un nom unique.
    /// Le nom des états ne sont pas mémorisés après l'écriture de l'automate.
    /// </remarks>
    public class AutomateTextWriter : IAutomateWriter
    {
        private readonly TextWriter _writer;
        private readonly IValueConverter<bool, string> _boolConverter;

        /// <summary>
        /// Initalise une nouvelle instance de la classe <see cref="AutomateTextWriter"/> avec le writer de texte spécifié
        /// et une nouvelle instance de <see cref="BooleanToBitStringConverter"/> comme convertisseur de valeurs.
        /// </summary>
        /// <param name="writer">Writer de texte.</param>
        public AutomateTextWriter(TextWriter writer)
            : this(writer, new BooleanToBitStringConverter())
        {
        }

        /// <summary>
        /// Initalise une nouvelle instance de la classe <see cref="AutomateTextWriter"/> avec le writer de texte spécifié
        /// et le convertisseur de valeurs spécifié.
        /// </summary>
        /// <param name="writer">Writer de texte.</param>
        /// <param name="boolConverter">Convertisseur de valeurs.</param>
        public AutomateTextWriter(TextWriter writer, IValueConverter<bool, string> boolConverter)
        {
            _writer = writer;
            _boolConverter = boolConverter ?? new BooleanToBitStringConverter();
        }

        /// <inheritdoc/>
        public void WriteAutomate(IAutomate automate)
        {
            if (_writer != null && automate != null)
            {
                var stateNames = new Dictionary<State, string>();
                WriteState(stateNames, automate.InitialState);
            }
        }

        /// <summary>
        /// Écrit récursivement un état et ses transitions dans le writer.
        /// </summary>
        /// <param name="stateNames">Noms des états.</param>
        /// <param name="state">État à écrire dans le writer.</param>
        private void WriteState(IDictionary<State, string> stateNames, State state)
        {
            if (state != null)
            {
                // Assigne un nom unique à l'état
                var stateName = string.Format("s{0}", stateNames.Count);
                stateNames[state] = stateName;

                // Écrit l'état
                _writer.WriteLine(string.Join(AutomateTextTokens.TokenSeparator, AutomateTextTokens.StateToken, stateName, _boolConverter.ConvertTo(state.IsFinal)));

                foreach (var transition in state.Transitions)
                {
                    if (!stateNames.ContainsKey(transition.ToState))
                    {
                        // Écrit récursivement les états manquants avant d'écrire la transition
                        WriteState(stateNames, transition.ToState);
                    }

                    // Écrit les transitions
                    _writer.WriteLine(string.Join(AutomateTextTokens.TokenSeparator, AutomateTextTokens.TransitionToken, stateName, transition.Input, stateNames[transition.ToState]));
                }
            }
        }
    }
}
