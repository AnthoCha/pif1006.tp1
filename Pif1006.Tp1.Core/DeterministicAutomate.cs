﻿using Pif1006.Tp1.Core.Interfaces;
using System.Linq;

namespace Pif1006.Tp1.Core
{
    /// <summary>
    /// Représente un automate déterministe.
    /// </summary>
    /// <remarks>
    /// Un automate déterministe n'accepte qu'au plus un état suivant par caractère d'entrée.
    /// </remarks>
    public class DeterministicAutomate : IAutomate
    {
        /// <inheritdoc/>
        public State InitialState { get; }

        /// <summary>
        /// Initalise une nouvelle instance de la classe <see cref="DeterministicAutomate"/> avec l'état initial spécifié.
        /// </summary>
        /// <param name="initialState">État initial de l'automate.</param>
        public DeterministicAutomate(State initialState)
        {
            InitialState = initialState;
        }

        /// <inheritdoc/>
        public bool IsValid(string input)
        {
            if (InitialState == null)
            {
                return false;
            }

            var currentState = InitialState;

            foreach (char currentChar in input)
            {
                // Pour un automate déterministe, seulement le premier état suivant pour un caractère d'entrée est considéré
                currentState = currentState.Transitions
                    .Where(transition => transition.Input == currentChar)
                    .Select(transition => transition.ToState)
                    .FirstOrDefault();

                if (currentState == null)
                {
                    return false;
                }
            }

            return currentState.IsFinal;
        }
    }
}
