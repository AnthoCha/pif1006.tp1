﻿namespace Pif1006.Tp1.Core.Interfaces
{
    /// <summary>
    /// Représente un lecteur capable de lire un automate.
    /// </summary>
    public interface IAutomateReader
    {
        /// <summary>
        /// Lit un automate du lecteur.
        /// </summary>
        /// <returns>Retourne l'automate suivant du lecteur.</returns>
        IAutomate ReadAutomate();
    }
}
