﻿namespace Pif1006.Tp1.Core.Interfaces
{
    /// <summary>
    /// Représente un writer capable d'écrire un automate.
    /// </summary>
    public interface IAutomateWriter
    {
        /// <summary>
        /// Écrit un automate dans le writer.
        /// </summary>
        /// <param name="automate">Automate à écrire dans le writer.</param>
        void WriteAutomate(IAutomate automate);
    }
}
