﻿namespace Pif1006.Tp1.Core.Interfaces
{
    /// <summary>
    /// Représente un automate capable de valider une chaine en entrée.
    /// </summary>
    public interface IAutomate
    {
        /// <summary>
        /// État initial de l'automate.
        /// </summary>
        State InitialState { get; }

        /// <summary>
        /// Évalue si la chaine en entrée est reconnu par l'automate. 
        /// </summary>
        /// <param name="input">Chaine en entrée.</param>
        /// <returns>Retourne true si la chaine en entrée est reconnu par l'automate, sinon false.</returns>
        bool IsValid(string input);
    }
}
