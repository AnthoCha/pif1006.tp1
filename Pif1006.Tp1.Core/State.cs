﻿using System.Collections.Generic;

namespace Pif1006.Tp1.Core
{
    /// <summary>
    /// Représente un état.
    /// </summary>
    public class State
    {
        /// <summary>
        /// Évalue si l'état est final.
        /// </summary>
        public bool IsFinal { get; }

        /// <summary>
        /// Transitions possible à partir de cet état.
        /// </summary>
        public ICollection<Transition> Transitions { get; }

        /// <summary>
        /// Initalise une nouvelle instance de la classe <see cref="State"/> avec l'état final spécifié et une collection de transitions vide.
        /// </summary>
        /// <param name="isFinal">Évalue si l'état est final.</param>
        public State(bool isFinal)
        {
            IsFinal = isFinal;
            Transitions = new List<Transition>();
        }
    }
}
