﻿namespace Pif1006.Tp1.Core
{
    /// <summary>
    /// Représente une transition.
    /// </summary>
    public class Transition
    {
        /// <summary>
        /// Caractère d'entrée.
        /// </summary>
        public char Input { get; }

        /// <summary>
        /// État suivant.
        /// </summary>
        public State ToState { get; }

        /// <summary>
        /// Initalise une nouvelle instance de la classe <see cref="Transition"/> avec le caractère d'entrée spécifié et l'état suivant spécifié.
        /// </summary>
        /// <param name="input">Caractère d'entrée.</param>
        /// <param name="toState">État suivant.</param>
        public Transition(char input, State toState)
        {
            Input = input;
            ToState = toState;
        }
    }
}
