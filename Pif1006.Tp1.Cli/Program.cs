﻿using Pif1006.Tp1.Core.Interfaces;
using Pif1006.Tp1.IO;
using System;
using System.IO;
using System.Text.RegularExpressions;

IAutomate _automate = null;

// Boucle principale pour le roulement de l'application
while (true)
{
    // Première intruction posée à l'utilisateur pour le chargement du fichier
    Console.Write("Veuillez entrer le nom du fichier qui contient l'automate : ");
    var path = Console.ReadLine();   
    var loop = true;
    try
    {
        // Chargement du fichier, création de l'automate et lecture de l'automate
        using var reader = File.OpenText(path);
        var automateReader = new DeterministicAutomateTextReader(reader);
        _automate = automateReader.ReadAutomate();
    }
    catch
    {
        Console.WriteLine("Fichier invalide.");
        loop = false;
    }
    // Boucle de menu 
    while (loop)
    {
        Console.Write("Veuillez choisir une option parmi les suivantes : " +
            "\n 1) Afficher" +
            "\n 2) Valider" +
            "\n 3) Changer d'automate" +
            "\n 4) Quitter" +
            "\n: ");

        var entry = Console.ReadLine();
        switch (entry)
        {
            case "1":
                // Affichage de l'automate 
                var automateWriter = new AutomateTextWriter(Console.Out);
                automateWriter.WriteAutomate(_automate);
                break;
            case "2":
                string isValid;
                string result;

                Console.Write("Veuillez entrer une chaine à faire valider par l'automate : ");
                isValid = Console.ReadLine();

                // Validation par expression régulière de l'entrée 
                result = Regex.IsMatch(isValid, @"^[0-1]*$") ? "Format accepté." : "Format invalide.";

                Console.WriteLine("{0}\nAutomate : {1}", result, _automate.IsValid(isValid) ? "Accepté" : "Refusé");
                break;
            case "3":
                loop = false;
                break;
            case "4":
                // Fin du programme
                Environment.Exit(0);
                break;
            default:
                Console.WriteLine("Choix invalide.");
                break;
        }
    }
}